describe PublicContentUploader do
  let(:public_content) { create :public_content }

  let(:uploader) { PublicContentUploader.new(public_content, :file) }

  before do
    PublicContentUploader.enable_processing = true
    File.open(File.join(Rails.root, '/spec/fixtures/documents/logo.png')) { |f| uploader.store!(f) }
  end

  after do
    PublicContentUploader.enable_processing = false
    uploader.remove!
  end

  it 'has the correct format' do
    expect(uploader.filename.split('.').last).to eql('png')
  end

end
