describe ImageUploader do
  let(:audit_area) { create :audit_area }

  let(:uploader) { ImageUploader.new(audit_area, :file) }

  before do
    ImageUploader.enable_processing = true
    File.open(File.join(Rails.root, '/spec/fixtures/documents/logo.png')) { |f| uploader.store!(f) }
  end

  after do
    ImageUploader.enable_processing = false
    uploader.remove!
  end

  it 'has the correct format' do
    expect(uploader.filename.split('.').last).to eql('png')
  end

  it 'has the correct size' do
    expect(uploader.file.size).to be <= 2*1024*1024
  end
end
