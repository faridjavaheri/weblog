require 'rails_helper'

RSpec.describe AuthorUserPolicy do
  subject { AuthorUserPolicy.new(user, author_user) }

  let(:author_user) { create :person }

  context 'for an admin' do
    let(:user) { create :admin_user }

    it { should permit(:index) }
    it { should permit(:show) }
    it { should permit(:update) }
  end

  context 'for an author' do
    let(:user) { create :author_user }

    it { should_not permit(:index) }
    it { should_not permit(:show) }
    it { should_not permit(:update) }
  end
end