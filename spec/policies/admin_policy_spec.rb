require 'rails_helper'

RSpec.describe AdminPolicy do
  subject { AdminPolicy.new(user, admin) }

  let(:admin) { create :admin }

  context 'for an admin' do
    let(:user) { create :admin_user }

    it { should permit(:index) }
    it { should permit(:show) }
    it { should permit(:create) }
    it { should permit(:update) }
    it { should permit(:edit) }
    it { should_not permit(:destroy) }
  end

  context 'for an author' do
    let(:user) { create :author_user }

    it { should_not permit(:index) }
    it { should_not permit(:show) }
    it { should_not permit(:create) }
    it { should_not permit(:update) }
    it { should_not permit(:edit) }
    it { should_not permit(:destroy) }
  end

end
