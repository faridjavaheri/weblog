# == Schema Information
#
# Table name: admins
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  first_name :string
#  last_name  :string
#  role       :integer
#  email      :string
#  password   :string
#

require 'rails_helper'
RSpec.describe Admin, type: :model do
  let(:admin_user) { create :admin_user }
  let(:admin) { admin_user.profile }

  describe 'validations' do
    it { is_expected.to validate_presence_of(:first_name) }
    it { is_expected.to validate_presence_of(:last_name) }
    it { is_expected.to validate_presence_of(:title) }
  end

  describe 'full_name' do
    it 'return user full name' do
      expect(admin.full_name).to eq("#{admin.first_name} #{admin.last_name}")
    end
  end

end
