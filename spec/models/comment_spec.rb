# frozen_string_literal: true
# == Schema Information
#
# Table name: comments
#
#  id          :integer          not null, primary key
#  description :string
#  post_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  title       :string
#

#


require 'rails_helper'
require 'spec_helper'

RSpec.describe Comment, type: :model do
  describe 'Assosciations' do
    it { should belong_to(:post) }
  end
end
