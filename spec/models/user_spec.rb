# == Schema Information
#
# Table name: users
#
#  id           :integer          not null, primary key
#  password     :string
#  role         :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  profile_id   :integer
#  profile_type :string
#  email        :string
#

# frozen_string_literal: true

require 'rails_helper'
require 'spec_helper'

RSpec.describe User, type: :model do
  context 'Enumerations' do
    it { is_expected.to respond_to :admin? }
    it { is_expected.to respond_to :author? }
    it { is_expected.to respond_to :guest? }
  end
end
