# == Schema Information
#
# Table name: admins
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  first_name :string
#  last_name  :string
#  role       :integer
#  email      :string
#  password   :string
#

FactoryBot.define do
  factory :admin, class:User do
    first_name { FFaker::Name.first_name }
    last_name { FFaker::Name.last_name }
    title { FFaker::Name.prefix }
  end
end
