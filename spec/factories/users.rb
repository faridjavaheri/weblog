# == Schema Information
#
# Table name: users
#
#  id           :integer          not null, primary key
#  password     :string
#  role         :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  profile_id   :integer
#  profile_type :string
#  email        :string
#

# frozen_string_literal: true

require 'ffaker'

FactoryBot.define do
  factory :user, class: User do
    sequence(:email) { FFaker::Internet.email }
    password { '12345@aA' }
    role { 'admin' }
    profile { |a| a.association(:admin) }
  end

  factory :admin_user, class: User do
    sequence(:email) { FFaker::Internet.email }
    password { '12345@aA' }
    role { 'admin' }
    profile { |a| a.association(:admin) }
  end

  factory :author_user, class: User do
    sequence(:email) { FFaker::Internet.email }
    password { '12345@aA' }
    role { 'author' }
    profile { |a| a.association(:author) }
  end

  factory :guest_user, class: User do
    sequence(:email) { FFaker::Internet.email }
    password { '12345@aA' }
    role { 'guest' }
    profile { |a| a.association(:guest) }
  end
end
