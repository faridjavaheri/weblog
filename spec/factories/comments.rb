# == Schema Information
#
# Table name: comments
#
#  id          :integer          not null, primary key
#  description :string
#  post_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  title       :string
#

FactoryBot.define do
  factory :comment do
    title { FFaker::Name.prefix }
    description { FFaker::Lorem.phrase }
  end
end
