# frozen_string_literal: true

require 'rails_helper'
require 'factory_bot'

describe Api::V1::AdminsController do
  let(:admin_user) { create :admin_user }
  let(:admin) { admin_user.profile }
  describe 'GET #index' do
    it 'returns a successful response' do
      # Send request
      get :index

      # Check expects
      expect(response).to be_successful
    end
  end

  describe 'GET #create' do
    it 'create a new admin' do
      # Requirements
      valid_admin_params = {
        first_name: 'test',
        last_name: 'test test',
        title: 'test title',
        user_attributes: {
          email: 'test@test2ewfdswer.com',
          password: '12345@aA'
        }
      }

      # Send request
      expect { post :create, params: valid_admin_params }
        .to change(Admin, :count).by(1)
    end
  end

  describe 'GET #show' do
    it 'returns a successful 200 response' do
      # Send request
      get :show, params: { id: admin.id }

      # Check expects
      expect(response).to be_successful
    end

    it 'returns data of an single admin user' do
      # Requirements
      user = create :admin_user

      # Send request
      get :show, params: { id: user.profile.id }
      json = JSON.parse(response.body)

      # Check expects
      expect(json['id'].to_i).to eq(user.profile.id)
      expect(json['first_name']).to eq(user.profile.first_name)
      expect(json['last_name']).to eq(user.profile.last_name)
      expect(json['title']).to eq(user.profile.title)
    end
  end

  describe 'PUT update' do
    context 'valid attributes' do
      it "changes admin_user's attributes" do
        # Requirements
        admin_user = create :admin_user, password: '12345@aA'
        valid_user_params = {
          id: admin_user.profile.id,
          first_name: 'test',
          last_name: admin_user.profile.last_name,
          title: 'test title',
          user_attributes: {
            email: 'test@updateuser123123.com',
            password: '12345@aA'
          }
        }

        # Send request
        put :update, params: valid_user_params

        # Check expects
        updated_admin_user = Admin.find(admin_user.profile.id).user
        expect(updated_admin_user.profile.first_name).to eq('test')
        expect(updated_admin_user.profile.last_name).to eq(admin_user.profile.last_name)
        expect(updated_admin_user.profile.title).to eq('test title')
        expect(updated_admin_user.email).to eq('test@updateuser123123.com')
      end
    end

    context 'invalid attributes' do
      it "does not change admin user's attributes" do
        # Requirements
        admin_user = create :admin_user, password: '12345@aA'
        old_email = admin_user.email
        old_first_name = admin_user.profile.first_name
        invalid_user_params = {
          id: admin_user.profile.id,
          first_name: 'newtest',
          last_name: admin_user.profile.last_name,
          title: 'test title',
          user_attributes: {
            email: 'test.com',
            password: ''
          }
        }

        # Send request
        put :update, params: invalid_user_params
        admin_user.reload

        # Check expects
        expect(admin_user.profile.first_name).to eq(old_first_name)
        expect(admin_user.email).to eq(old_email)
      end
    end
  end
end
