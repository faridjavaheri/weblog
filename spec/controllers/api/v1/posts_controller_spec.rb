# frozen_string_literal: true
require 'rails_helper'
require 'factory_bot'

describe Api::V1::PostsController do
  describe 'GET #index' do
    it 'returns a successful response' do
      # Send request
      get :index

      # Check expects
      expect(response).to be_successful
    end
  end

  describe 'GET #create' do
    it 'create a new post' do
      # Requirements
      valid_post_params = {
        title: 'test',
        description: 'test test'
      }

      # Send request
      expect { post :create, params: valid_post_params }.to change(Post, :count).by(1)
    end
  end

  describe 'GET #show' do
    it 'returns a successful 200 response' do
      # Requirements
      post = create :post

      # Send request
      get :show, params: { id: post.id }

      # Check expects
      expect(response).to be_successful
    end
  end

  describe 'PUT update' do
    it 'changes attributes of post' do
      # Requirements
      post = create :post
      valid_post = {
        id: post.id,
        title: 'new test'
      }

      # Send request
      put :update, params: valid_post
      post.reload

      # Check expects
      expect(post.title).to eq('new test')
    end
  end

  describe 'GET #destroy' do
    it 'deletes the post' do
      # Requirements
      product1 = create :product
      post = create :post, product: product1
      post_count = Post.count

      # Send request
      delete :destroy, params: { id: post.id }

      # Check expects
      expect(Post.count).to eq(post_count - 1)
    end
  end
end