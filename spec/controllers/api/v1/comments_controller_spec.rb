# frozen_string_literal: true
require 'rails_helper'
require 'factory_bot'

describe Api::V1::CommentsController do
  describe 'GET #index' do
    it 'returns a successful response' do
      # Send request
      get :index

      # Check expects
      expect(response).to be_successful
    end
  end

  describe 'GET #create' do
    it 'create a new comment' do
      # Requirements
      valid_comment_params = {
        title: 'test',
        description: 'test test'
      }

      # Send request
      expect { post :create, params: valid_comment_params }.to change(Comment, :count).by(1)
    end
  end

  describe 'GET #show' do
    it 'returns a successful 200 response' do
      # Requirements
      comment = create :comment

      # Send request
      get :show, params: { id: comment.id }

      # Check expects
      expect(response).to be_successful
    end
  end

  describe 'PUT update' do
    it 'changes attributes of comment' do
      # Requirements
      comment = create :comment
      valid_comment = {
        id: comment.id,
        title: 'new test'
      }

      # Send request
      put :update, params: valid_comment
      comment.reload

      # Check expects
      expect(comment.title).to eq('new test')
    end
  end

  describe 'GET #destroy' do
    it 'deletes the comment' do
      # Requirements
      product1 = create :product
      comment = create :comment, product: product1
      comment_count = Comment.count

      # Send request
      delete :destroy, params: { id: comment.id }

      # Check expects
      expect(Comment.count).to eq(comment_count - 1)
    end
  end
end