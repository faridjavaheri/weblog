# frozen_string_literal: true
require 'spec_helper'
require 'rails_helper'

RSpec.describe SessionsController, type: :controller do
  before(:each) do
    @request.env['devise.mapping'] = Devise.mappings[:user]
  end

  context 'POST #create' do
    # Successfull Action For Admin
    it 'successfully authenticate for Admin User' do
      create :admin_user, email: 'adminuser@test.com', password: '12345@aA'

      post :create, params: { user: { email: 'adminuser@test.com', password: '12345@aA' } }

      expect(response.status).to eq(201)
    end

    # Successfull Action For expert
    it 'successfully authenticate for expert User' do
      create :expert_user, email: 'expertuser@test.com', password: '12345@aA'

      post :create, params: { user: { email: 'expertuser@test.com', password: '12345@aA' } }

      expect(response.status).to eq(201)
    end

    # Fail Action For Wrong User
    it 'returns a faild login response' do
      post :create, params: { user: { email: 'faildemail@test.com', password: '12345@aA' } }

      expect(response.status).to eq(401)
    end
  end

  context 'DELETE #response_to_on_destroy' do
    # Successfull Action for logout
    it 'returns a successful logout response' do
      admin_user = create(:admin_user, email: 'adminuser@test.com', password: '12345@aA')
      sign_in(admin_user)

      delete :destroy

      expect(response.status).to eq(200)
    end
  end
end