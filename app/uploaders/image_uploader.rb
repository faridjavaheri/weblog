# frozen_string_literal: true

class ImageUploader < CarrierWave::Uploader::Base
  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def size_range
    0..2.megabytes
  end

  def extension_whitelist
    %w(jpg jpeg png svg)
  end

  def filename
    "#{secure_token}.#{file.extension}" if original_filename.present?
  end

protected

  def secure_token
    var = :"@#{mounted_as}_secure_token"
    model.instance_variable_get(var) || model.instance_variable_set(var, SecureRandom.uuid)
  end
end
