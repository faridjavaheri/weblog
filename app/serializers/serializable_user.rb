# frozen_string_literal: true

# User serializer
class SerializableUser < JSONAPI::Serializable::Resource
  type 'user'

  attributes :password, :role, :email, :profile_id, :profile_type
end
