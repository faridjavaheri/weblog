# frozen_string_literal: true

# Admin serializer
class SerializableAdmin < JSONAPI::Serializable::Resource
  type 'admin'

  attributes :first_name, :last_name, :title, :role
end
