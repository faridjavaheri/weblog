# frozen_string_literal: true

# Post serializer
class SerializablePost < JSONAPI::Serializable::Resource
  type 'post'

  attributes :title, :description
end
