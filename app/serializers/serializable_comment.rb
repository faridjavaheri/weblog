# frozen_string_literal: true
# Comment serializer
class SerializableComment < JSONAPI::Serializable::Resource
  type 'comment'

  attributes :title, :description, :post_id

  belongs_to :post
end