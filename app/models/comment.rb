# == Schema Information
#
# Table name: comments
#
#  id          :integer          not null, primary key
#  description :string
#  post_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  title       :string
#

# Comment model
class Comment < ApplicationRecord
  belongs_to :post, class_name: 'Post', optional: true
end

