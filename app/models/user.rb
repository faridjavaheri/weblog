# == Schema Information
#
# Table name: users
#
#  id           :integer          not null, primary key
#  password     :string
#  role         :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  profile_id   :integer
#  profile_type :string
#  email        :string
#

# frozen_string_literal: true

# User Model
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  enum role: %i[admin author guest]

  belongs_to :profile, polymorphic: true, optional: true

  def an_admin?
    role == 'admin'
  end

  def a_expert?
    role == 'expert'
  end

  def a_guest?
    role == 'guest'
  end

  # admin roles
  def admin?
    role == 'admin'
  end

  # expert roles
  def expert?
    role == 'expert'
  end

  # guest roles
  def guest?
    role == 'guest'
  end
end
