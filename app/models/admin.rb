# == Schema Information
#
# Table name: admins
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  first_name :string
#  last_name  :string
#  role       :integer
#  email      :string
#  password   :string
#

class Admin < ApplicationRecord
  has_one :user, as: :profile, dependent: :destroy

  validates :first_name, :last_name, :title, presence: true

  delegate :role, :email, to: :user

  accepts_nested_attributes_for :user

  def full_name
    "#{first_name} #{last_name}".strip
  end

end
