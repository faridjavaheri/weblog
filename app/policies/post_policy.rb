class AdminPolicy < ApplicationPolicy
  def index?
    user.admin? || user.author? || user.guest?
  end

  def show?
    user.admin?
  end

  def create?
    user.admin?
  end

  def edit?
    user.admin?
  end

  def update?
    user.admin?
  end

  def update_with_password?
    update?
  end

  def destroy?
    false
  end
end
