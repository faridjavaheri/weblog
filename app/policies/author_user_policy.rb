class AuthorPolicy < ApplicationPolicy
  def index?
    user.author?
  end

  def show?
    user.author?
  end

  def create?
    user.author?
  end

  def edit?
    user.author?
  end

  def update?
    user.author?
  end

  def update_with_password?
    update?
  end

  def destroy?
    false
  end
end
