# frozen_string_literal: true

module Api
  module V1
    # Postscontroller
    class PostsController < ApplicationController
      before_action :set_post, only: %i[show edit update]
      before_action :validate_params, only: :update

      # GET /Posts
      def index
        @posts = Post.all
        render jsonapi: @posts
      end

      # GET /posts/1
      def show
        render json: @post
      end

      # POST /posts
      def create
        @post = Post.new(post_params)
        @post.user.role = 'post'
        if @post.save
          render jsonapi: @post, status: :created
        else
          render json: @post.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /posts/1
      def update
        if @post.update(post_params)
          render json: @post
        else
          render json: @post.errors, status: :unprocessable_entity
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_post
        @post = Post.find(params[:id])
      end

      # Only allow a trusted parameter “white list” through.
      def post_params
        params.permit(
          :first_name, :last_name, :title,
          user_attributes: [
            :id, :email, :password
          ]
        )
      end

      def validate_params
        user = @post.user
        params[:user_attributes][:id] = user.id

        if params[:user_attributes][:password]
          params[:user_attributes].delete(:password)
        end
      end
    end
  end
end
