# frozen_string_literal: true

module Api
  module V1
    # Commentscontroller
    class CommentsController < ApplicationController
      before_action :set_comment, only: %i[show edit update]
      before_action :validate_params, only: :update

      # GET /Comments
      def index
        @comments = Comment.all
        render jsonapi: @comments
      end

      # GET /comments/1
      def show
        render json: @comment
      end

      # POST /comments
      def create
        @comment = Comment.new(comment_params)
        @comment.user.role = 'comment'
        if @comment.save
          render jsonapi: @comment, status: :created
        else
          render json: @comment.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /comments/1
      def update
        if @comment.update(comment_params)
          render json: @comment
        else
          render json: @comment.errors, status: :unprocessable_entity
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_comment
        @comment = Comment.find(params[:id])
      end

      # Only allow a trusted parameter “white list” through.
      def comment_params
        params.permit(
          :first_name, :last_name, :title,
          user_attributes: [
            :id, :email, :password
          ]
        )
      end

      def validate_params
        user = @comment.user
        params[:user_attributes][:id] = user.id

        if params[:user_attributes][:password]
          params[:user_attributes].delete(:password)
        end
      end
    end
  end
end
