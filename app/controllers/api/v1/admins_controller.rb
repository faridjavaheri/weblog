# frozen_string_literal: true

module Api
  module V1
    # Adminscontroller
    class AdminsController < ApplicationController
      before_action :set_admin, only: %i[show edit update]
      before_action :validate_params, only: :update

      # GET /Admins
      def index
        authorize(Admin)
        @admins = Admin.all
        render jsonapi: @admins
      end

      # GET /admins/1
      def show
        authorize(Admin)
        render json: @admin
      end

      # POST /admins
      def create
        authorize(Admin)
        @admin = Admin.new(admin_params)
        @admin.user.role = 'admin'
        if @admin.save
          render jsonapi: @admin, status: :created
        else
          render json: @admin.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /admins/1
      def update
        if @admin.update(admin_params)
          render json: @admin
        else
          render json: @admin.errors, status: :unprocessable_entity
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_admin
        @admin = Admin.find(params[:id])
        authorize @admin
      end

      # Only allow a trusted parameter “white list” through.
      def admin_params
        params.permit(
          :first_name, :last_name, :title,
          user_attributes: [
            :id, :email, :password
          ]
        )
      end

      def validate_params
        user = @admin.user
        params[:user_attributes][:id] = user.id

        if params[:user_attributes][:password]
          params[:user_attributes].delete(:password)
        end
      end
    end
  end
end
