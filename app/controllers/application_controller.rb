# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  before_action :redirection_status, only: :auth
  before_action :authenticate_user!, only: %i(admins authors)
  before_action :check_user_status, only: :auth
  protect_from_forgery with: :exception
  before_action :set_locale
  before_action :set_paper_trail_whodunnit, only: %i(admins sale_reps dpos authors)

  # Entry point Admins App
  def admins
    skip_authorization

    redirect_unless('admin') && return
    @user_asstes_name = 'admins'
    render template: 'layouts/application'
  end

  # Entry point SaleReps App
  def authors
    skip_authorization

    redirect_unless('author') && return
    @user_asstes_name = 'authors'
    render template: 'layouts/application'
  end

  # Entry point Auth
  def auth
    skip_authorization

    render template: 'layouts/application'
  end

private

  def set_locale
    I18n.locale = params[:locale] || session[:locale] || I18n.default_locale
    session[:locale] = I18n.locale
  end

  def redirection_status
    session[:do_not_redirect_to_audit] = params[:redirect] == 'false'
  end

  def user_not_authorized
    message = 'You are not authorized to perform this action.'
    render json: { error: message }, status: :unauthorized
  end

  def authenticate_user!
    return super if user_signed_in?
    return super if controller_name == 'authors'

    redirect_to auth_path
  end

  def check_user_status
    return unless user_signed_in?

    redirect_to send("#{current_user.role}_path".to_sym)
  end

  def redirect_unless(role)
    redirect_to(auth_path) && return unless current_user

    unless current_user.role == role.to_s
      redirect_to send("#{current_user.role}_path".to_sym)
    end
  end
end
