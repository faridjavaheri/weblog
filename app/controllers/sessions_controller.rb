# frozen_string_literal: true

class SessionsController < Devise::SessionsController
  def new
    message = I18n.t 'devise.failure.locked'
    render json: { error: message }, status: :unauthorized
  end

  def create
    session['user_auth'] = params[:user]
    resource = warden.authenticate!(scope: resource_name, recall: 'sessions#failure')
    sign_in(resource_name, resource)

    message = I18n.t 'devise.sessions.signed_in'

    render json: { message: message, data: current_user }, status: :created
  end

  def failure
    message = I18n.t 'devise.failure.invalid', authentication_keys: 'email'
    render json: { error: message }, status: :unauthorized
  end

  def respond_to_on_destroy
    message = I18n.t 'devise.sessions.signed_out'
    render json: { message: message }, status: :ok
  end
end
