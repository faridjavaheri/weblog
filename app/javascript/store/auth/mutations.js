import * as types from './mutation_types'

export default {
  [types.SET_PAGE_TITLE](state, data) {
    state.pageTitle = data
  },
  [types.SET_AUTH_ERRORS](state, data) {
    state.authErrors = {
      errors: data.errors,
      server: data.server,
      type: data.type !== undefined ? data.type : data.type = 'error',
    }
  },
}
