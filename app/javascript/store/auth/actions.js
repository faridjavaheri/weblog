import * as types from './mutation_types'

export default {
  setAuthErrors (
    { commit, state },
    { errors, server, type }
  ) {
    commit(types.SET_AUTH_ERRORS, { errors, server, type })
  },

  setPageTitle (
    { commit, state },
    { data }
  ) {
    commit(types.SET_PAGE_TITLE, { data })
  },
}
