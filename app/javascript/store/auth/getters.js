export default {
  getApiRoute (state) {
    return state.api_route
  },
  getAuthErrors (state) {
    return state.authErrors
  },
  getPageTitle (state) {
    return state.pageTitle
  },
}
