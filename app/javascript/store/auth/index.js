import Vue from 'vue'
import mutations from './mutations'
import actions from './actions'
import getters from './getters'


export default new Vuex.Store({
  getters,
  mutations,
  actions,
  state: {
    api_route: '/api/v1/',
    authErrors: {
      errors: [],
      server: false,
      type: 'error',
    },
    pageTitle: '',
  },
})
