import Vue from 'vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import globalMethods from './packs/shared/global_methods';
import ElementUI from 'element-ui';

import {
  faCheck,
  faTimes,
  faExclamationCircle,
  faSitemap,
  faEdit,
  faEye,
  faTrashAlt,
  faChevronLeft,
  faPlusCircle,
  faEnvelope,
  faFlag,
  faPhone,
  faHistory,
} from '@fortawesome/free-solid-svg-icons';

require('fake-indexeddb/auto');

Vue.mixin(globalMethods);
Vue.use(ElementUI);
Vue.component('font-awesome-icon', FontAwesomeIcon);
library.add(
  faCheck,
  faTimes,
  faExclamationCircle,
  faSitemap,
  faEdit,
  faEye,
  faTrashAlt,
  faChevronLeft,
  faPlusCircle,
  faEnvelope,
  faFlag,
  faPhone,
  faHistory
);