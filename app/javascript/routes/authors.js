import Menu from '../panels/authors/menu.vue'
import Welcome from '../panels/authors/welcome.vue'

const routes = [
  {
    path: '/welcome',
    name: 'index',
    components: { default: Welcome, menu: Menu },
  },
  {
    path: '/',
    name: 'main',
    components: { default: Welcome, menu: Menu },
  },
]

export default routes
