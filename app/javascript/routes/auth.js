import Auth from '../panels/auth/auth.vue'

const routes = [
  { path: '/',
    name: 'auth',
    components: {
      default: Auth,
    },
  },
]

export default routes
