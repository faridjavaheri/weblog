import Menu from '../components/menu/menu.vue'
import Index from '../panels/admins/index.vue'
import Signin from '../components/sign_in.vue'

import UserManagement from '../components/user/layout.vue'
import UserIndex from '../components/user/index.vue'
import UserForm from '../components/user/form.vue'
import UserShow from '../components/user/show.vue'


import AuthorUserLayout from '../components/author_user/layout.vue'
import AuthorUserForm from '../components/author_user/form.vue'
import AuthorUserShow from '../components/author_user/show.vue'
import AuthorUserIndex from '../components/author_user/index.vue'

const routes = [
  { path: '/', components: { default: Index, menu: Menu } },
  { path: '/users/:userType',
    components: { default: UserManagement, menu: Menu },
    children: [
      {
        path: '',
        name: 'user_index',
        meta: { name: 'user' },
        component: UserIndex,
      },
      {
        path: 'new',
        name: 'user_new',
        meta: { name: 'user' },
        component: UserForm,
        props: { edit: false },
      },
      {
        path: ':id/edit',
        name: 'user_edit',
        meta: { name: 'user' },
        component: UserForm,
        props: { edit: true },
      },
      {
        path: ':id',
        name: 'user_show',
        meta: { name: 'user' },
        component: UserShow,
      },
    ],
  },
  { path: '/author_users',
    components: { default: AuthorUserLayout, menu: Menu },
    children: [
      {
        path: '',
        name: 'author_user_index',
        meta: { name: 'author_users' },
        component: AuthorUserIndex,
      },
      {
        path: ':id/edit',
        name: 'author_user_edit',
        meta: { name: 'author_users' },
        component: AuthorUserForm,
        props: { edit: true },
      },
      {
        path: ':id',
        name: 'author_user_show',
        meta: { name: 'author_users' },
        component: AuthorUserShow,
      },
    ],
  },
    {
      path: '/sign_in',
      name: 'sign_in',
      meta: { name: 'sign_in' },
      component: Signin,
    }
]

export default routes
