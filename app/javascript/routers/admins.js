import routes from '../routes/admins'
import VueRouter from 'vue-router'

const router = new VueRouter({
  mode: 'history',
  base: '/admins/',
  routes,
})

export default router
