import routes from '../routes/authors'
import VueRouter from 'vue-router'

const router = new VueRouter({
  mode: 'history',
  base: '/authors/',
  routes,
})

export default router
