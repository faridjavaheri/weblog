Rails.application.config.generators do |g|
  g.helper false
  g.assets false
  g.javascripts false
  g.stylesheets false
  g.template_engine :slim
end
