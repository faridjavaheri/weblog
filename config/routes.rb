# frozen_string_literal: true

Rails.application.routes.draw do

  devise_for :users
  namespace :api do
    namespace :v1 do
      resources :admins
    end
  end
  # devise_for :admins
  get '/admin/(*path)', to: 'application#admin'
  get '/(*path)', to: 'application#auth'

  root to: "auth#index"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
